# Newsflow
Version 3.7, Open Source Saas Service

## The development, test and production foundation
* Bedrock
* WordPress Multisite installation

## Continuous Integration
* Gitlab runners
* Style CI
* WP-CLI
* Deployer
* Docker-Bedrock

## Plugins
* Bladerunner

## Services+

