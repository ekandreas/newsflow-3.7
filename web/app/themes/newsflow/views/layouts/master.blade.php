<!DOCTYPE html>
<html>
@include('views.parts.head')
<body {{ body_class('hold-transition skin-blue sidebar-mini') }}>
  @include('views.parts.ga')
<div class="wrapper">

  @include('views.parts.header')

  @include('views.parts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @include('views.contents.header')

    <!-- Main content -->
    <section class="content">

      @yield('contents')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('views.parts.footer')

  @include('views.parts.control')

</div>
<!-- ./wrapper -->

@include('views.parts.scripts')

</body>
</html>
