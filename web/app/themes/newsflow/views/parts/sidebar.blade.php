<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ assets('images/aek.jpg') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Andreas Ek</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Elseif AB</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Sök...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENY</li>
      <li class="active treeview">
        <a href="/">
          <i class="fa fa-dashboard"></i> <span>Hem</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="/"><i class="fa fa-circle text-success"></i> Start</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-info"></i>
          <span>Support</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="https://trello.com/b/6w2XApXR/newsflow" target="_blank"><i class="fa fa-briefcase" aria-hidden="true"></i> Pågående arbete: Trello</a></li>
          <li><a href="https://newsflowio.slack.com" target="_blank"><i class="fa fa-slack" aria-hidden="true"></i> Slack</a></li>
          <li><a href="https://gitlab.com/ekandreas/newsflow" target="_blank"><i class="fa fa-code"></i> Kodrepo</a></li>
          <li><a href="mailto:andreas@elseif.se"><i class="fa fa-envelope-o"></i> E-postmeddelande</a></li>
        </ul>
      </li>


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
