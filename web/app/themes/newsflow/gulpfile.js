var elixir = require('laravel-elixir');

config.assetsPath='assets';
config.publicPath='dist';

elixir(function(mix) {

    mix.less('app.less');

    mix.scripts([
        'app.js',
        './bower_components/jquery/dist/jquery.js', 
        './bower_components/bootstrap/dist/js/bootstrap.js', 
        './bower_components/vue/dist/vue.js', 
        './bower_components/AdminLTE/dist/js/app.js', 
        'unauthenticated.js', 
        'usergate.js', 
    ]);

    mix.copy([
    	'./bower_components/font-awesome/fonts/*.*',
    	'./assets/fonts',
    	], 'dist/fonts');

    mix.copy('./assets/images', 'dist/images');

});