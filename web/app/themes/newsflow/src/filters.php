<?php
add_action('wp_enqueue_scripts', 'Newsflow\UI\Assets::enqueueScripts');

add_filter('show_admin_bar', function () {
    if (!is_super_admin()) {
        return false;
    } else {
        return true;
    }
});

/* UserGate */
add_action('template_redirect', 'Newsflow\UI\UserGate::template_redirect');
add_filter('query_vars', 'Newsflow\UI\UserGate::query_vars');
add_filter('template_include', 'Newsflow\UI\UserGate::template_include', 9);
add_action('wp_ajax_getUserUrl', 'Newsflow\UI\UserGate::getUserUrl');

// this can't be inside the theme source since it's not loaded at init.
add_action('init', function () {
    add_rewrite_rule('^usergate/?', 'index.php?usergate=true', 'top');
});

// AdminMenu
add_action('admin_menu', 'Newsflow\Admin\Menu::menues');
add_action('parent_file', 'Newsflow\Admin\Menu::parent_file');
add_filter('page_row_actions', 'Newsflow\Admin\Menu::add_fetch_link', 10, 2);

//Inbox
add_action('wp_ajax_newsflow_opml_export', 'Newsflow\Admin\Inbox::opml_export');
add_action('wp_ajax_newsflow_clear_items', 'Newsflow\Admin\Inbox::clear_items');
add_action('wp_ajax_newsflow_out_of_bounds', 'Newsflow\Admin\Inbox::out_of_bounds');

//Cpt
add_action('init', 'Newsflow\Structures\Cpt::register');

// Papi
add_filter('papi/settings/directories', function ($directories) {
    $directories[] = realpath(dirname(__FILE__) . '/page-types');
    return $directories;
});

add_action('save_post', 'Newsflow\Sources\Save::post');

// Jobs
add_action('wp_ajax_newsflow', 'Newsflow\Cron\Job::ajaxRun');
add_action('newsflow/job', 'Newsflow\Cron\Job::run');
add_filter('cron_schedules', 'Newsflow\Cron\Job::addNewIntervals');
add_action('admin_init', 'Newsflow\Cron\Job::addJob');
