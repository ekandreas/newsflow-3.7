<?php
namespace Newsflow\Admin;

use papi;

class Menu
{
    public static function add_fetch_link($actions, $page_object)
    {
        if ($page_object->post_type == "newsflow-source") {
            $actions['fetch_link'] = '<a target="_blank" href="' . admin_url('admin-ajax.php') . '?action=newsflow-run&id=' . $page_object->ID . '">Fetch</a>';
        }
        return $actions;
    }

    public static function menues()
    {
        add_menu_page('Newsflow', 'Newsflow', 'edit_others_posts', 'newsflow-admin-inbox',
            __NAMESPACE__ . '\Inbox::display', '', 3);

        add_submenu_page('newsflow-admin-inbox', 'Sources', 'Sources', 'edit_others_posts',
            'edit.php?post_type=source');
        add_submenu_page('newsflow-admin-inbox', 'Categories', 'Categories', 'edit_others_posts',
            'edit-tags.php?taxonomy=category');
        add_submenu_page('newsflow-admin-inbox', 'Channels', 'Channels', 'edit_others_posts',
            'edit-tags.php?taxonomy=channel');
        add_submenu_page('newsflow-admin-inbox', 'Contracts', 'Contracts', 'edit_others_posts',
            'edit-tags.php?taxonomy=contract');
        add_submenu_page('newsflow-admin-inbox', 'Configurations', 'Configurations', 'edit_others_posts',
            'edit.php?post_type=config');
    }

    public static function parent_file($parent_file)
    {
        global $current_screen;
        $taxonomy  = $current_screen->taxonomy;
        $post_type = $current_screen->post_type;
        $papi_page = papi_get_page_type_id(get_the_ID());
        if (
            $post_type == 'source' ||
            $post_type == 'config' ||
            $taxonomy == 'category' ||
            $taxonomy == 'channel' ||
            $taxonomy == 'contract' ||
            $papi_page == 'feed' ||
            $papi_page == 'magic'
        ) {
            $parent_file = 'newsflow-admin-inbox';
        }

        return $parent_file;
    }
}
