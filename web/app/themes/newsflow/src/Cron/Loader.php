<?php
namespace Newsflow\Cron;

class Loader
{
    private $sources;

    public function loadSources($max_active = 1)
    {
        $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : 0;

        if (! $id) {
            $args = [
                'post_type'      => \Newsflow\Sources\Base::POST_TYPE,
                'posts_per_page' => $max_active,
                'meta_query'     => array(
                    array(
                        'key'     => 'status',
                        'compare' => 'NOT EXISTS',
                    ),
                ),
                'order'          => 'ASC',
                'meta_key'       => 'fetched',
                'orderby'        => 'meta_value_num',
            ];

            $sources = get_posts($args);

            if ($sources) {
                foreach ($sources as $source) {
                    $this->sources[] = $source;
                }
            }
        } else {
            $source = get_post($id);
            if ($source) {
                $this->sources[] = $source;
            }
        }
    }

    public function fetch()
    {
        if ($this->sources && sizeof($this->sources)) {
            foreach ($this->sources as $post) {
                $source = Loader::get_source_object($post);

                try {
                    $source->mark_started();

                    $source->pre_fetch();
                    $source->fetch();
                    $source->post_fetch();
                } catch (Exception $ex) {
                }
            }
        }
    }

    public static function get_source_object($post)
    {
        if (is_numeric($post)) {
            $post = \get_post($post);
        }

        $page_type = papi_get_page_type_id($post->ID);

        if ($page_type == 'feed') {
            return new \Newsflow\Sources\Feed($post);
        }
        if ($page_type == 'magic') {
            return new \Newsflow\Sources\Magic($post);
        }
        if ($page_type == 'kimono') {
            return new \Newsflow\Sources\Kimono($post);
        }

        throw new \Exception('Base class for type ' . $page_type . ' is missing in Newsflow!');
    }
}
