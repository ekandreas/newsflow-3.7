<?php
namespace Newsflow\Cron;

class Job
{
    public static function run()
    {
        Job::blockedFetch();

        $loader = new Loader();
        $loader->loadSources();
        $loader->fetch();

        Job::clearOutOfBounds();
    }

    public static function ajaxRun()
    {
        Job::run();
        die;
    }

    public static function addNewIntervals($schedules)
    {
        $schedules['minute'] = array(
            'interval' => 60,
            'display'  => __('Minute')
        );

        return $schedules;
    }

    public static function addJob()
    {
        if (! wp_next_scheduled('newsflow/job')) {
            wp_schedule_event(current_time('timestamp', true), 'minute', 'newsflow/job');
        }
    }


    public static function clearOutOfBounds()
    {
        $posts = get_posts([
            'post_type'      => 'post',
            'posts_per_page' => 100,
            'meta_key'       => 'touched',
            'meta_compare'   => '<',
            'meta_type'      => 'NUMERIC',
            'meta_value'     => time() - 86400,
            'post_status'    => 'any',
        ]);

        foreach ($posts as $key => $post) {
            $source = get_post(papi_get_field($post->ID, 'source_id'));
            $out_of_bounds_matched = (int)papi_get_field($source->ID, 'out_of_bounds_matched');
            $out_of_bounds_unmatched = (int)papi_get_field($source->ID, 'out_of_bounds_unmatched');
            $touched = papi_get_field($post->ID, 'touched');

            $current_out_of_bounds = time() - $touched;

            $matched = $post->post_status == 'publish' ? true : false;

            $delete = false;

            //echo "<span";

            if ($matched && $current_out_of_bounds > $out_of_bounds_matched) {
                //echo ' style="text-decoration:line-through"';
                $delete = true;
            }

            if (!$matched && $current_out_of_bounds > $out_of_bounds_unmatched) {
                //echo ' style="text-decoration:line-through"';
                $delete = true;
            }

            if ($delete) {
                wp_delete_post($post->ID);
            } else {
                papi_update_field($post->ID, 'touched', time());
            }
        }
    }

    public static function blockedFetch()
    {
        $posts = get_posts([
            'post_type'      => 'source',
            'posts_per_page' => 500,
            'meta_query' => [
                [
                    'key' => 'status',
                    'value' => 'FETCHING',
                    'compare' => '=',
                ],
                [
                    'key' => 'fetched',
                    'value' => time()-300,
                    'compare' => '<',
                    'type' => 'NUMERIC',
                ]
            ]
        ]);

        foreach ($posts as $key => $post) {
            $fetched = papi_get_field($post->ID, 'fetched');
            papi_delete_field($post->ID, 'status');
        }
    }
}
