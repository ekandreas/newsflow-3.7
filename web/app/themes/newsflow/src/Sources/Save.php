<?php
namespace Newsflow\Sources;

class Save
{
    public static function Post($post_id)
    {
        if (wp_is_post_revision($post_id) ||
            wp_is_post_autosave($post_id) ||
            (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
            ! isset($_POST['post_type']) ||
            Base::POST_TYPE != $_POST['post_type']
        ) {
            return $post_id;
        }

        if (papi_get_field($post_id, 'delete')) {
            $posts = get_posts([
                'post_type'      => 'post',
                'post_status'    => get_available_post_statuses('post'),
                'meta_key'       => 'source_id',
                'meta_value'     => $post_id,
                'posts_per_page' => - 1,
            ]);
            if ($posts) {
                foreach ($posts as $post) {
                    wp_delete_post($post->ID, true);
                }
            }
            papi_delete_field($post_id, 'delete');
        }

        return $post_id;
    }
}
