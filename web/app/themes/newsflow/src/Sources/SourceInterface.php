<?php
namespace Newsflow\Sources;

interface SourceInterface
{
    public function fetch();
}
