<?php
namespace Newsflow\Sources;

class Post
{
    const POST_TYPE = 'post';
    const PAGE_TYPE = 'post';

    public $unique_id;
    public $title;
    public $link;
    public $content;
    public $date;
    public $item_checksum;
    public $post_id;
    public $thumbnail;

    private $is_item_dirty;

    public $feed_post;

    public $post_data;

    protected $matching_posts;
    protected $replacement_posts;

    public function __construct($id, $title, $content, $link, $date, $thumbnail)
    {
        $this->post_id       = 0;
        $this->is_item_dirty = true;
        $this->thumbnail     = null;

        $this->unique_id = $id;
        $this->post_id   = $this->existing_post_id($id);

        $this->title   = $title;
        $this->content = $content;
        $this->link    = $link;
        $this->thumbnail = $thumbnail;

        if (strtotime($date) > time()) {
            $date = time();
        }

        // preserve date at old post
        if (!isset($post_id)) {
            $this->date = date_i18n('Y-m-d H:i:s', strtotime($date));
        } else {
            $this->date = \get_the_date('Y-m-d H:i:s', $post_id);
        }

        $this->check_dirty();
    }

    public function check_dirty()
    {
        if ($this->post_id) {
            $checksum     = $this->get_checksum();
            $old_checksum = (int) papi_get_field($this->post_id, 'checksum');
            if ($checksum == $old_checksum) {
                $this->is_item_dirty = false;
            } else {
                papi_update_field($this->post_id, 'checksum', $checksum);
            }
        } else {
            $this->is_item_dirty = true;
        }
    }


    public function is_dirty()
    {
        return $this->is_item_dirty;
    }

    public function set_dirty()
    {
        $this->is_item_dirty = true;
    }

    public function get_checksum()
    {
        $checksum_string = $this->title . $this->content . $this->link;

        return crc32($checksum_string);
    }

    public function persist($publish = true)
    {
        if (filter_var($this->thumbnail, FILTER_VALIDATE_URL)) {
            if (!(int)@getimagesize($this->thumbnail)) {
                $this->thumbnail = null;
            }
        } else {
            $this->thumbnail=null;
        }

        $this->post_data = [
            'post_title'   => $this->title,
            'post_content' => $this->content,
            'post_status'  => $publish ? 'publish' : 'unmatched',
            'post_type'    => $this::POST_TYPE,
        ];

        if (! $this->post_id) {
            if (strtotime($this->date) < time()) {
                $this->post_data['post_date'] = $this->date;
            }
            $this->post_id = $this->insert_post();
            $this->set_meta($this->post_id);
        } else {
            $existing_checksum = (int) papi_get_field($this->post_id, 'checksum');

            //if ($this->is_item_dirty || $existing_checksum !== $this->get_checksum()) {
            $this->update_post($this->post_id);
            $this->set_meta($this->post_id);
            //}
        }

        $this->set_taxonomies($this->feed_post->ID);
    }

    public function set_taxonomies($parent_post_id)
    {
        $categories = wp_get_object_terms($parent_post_id, 'category');
        if ($categories && sizeof($categories)) {
            $cats = [ ];
            foreach ($categories as $category) {
                $cats[] = $category->term_id;
            }
            wp_set_post_terms($this->post_id, $cats, 'category', true);
        }

        $channels = wp_get_object_terms($parent_post_id, 'channel');
        if ($channels && sizeof($channels)) {
            $chans = [ ];
            foreach ($channels as $channel) {
                $chans[] = $channel->term_id;
            }
            wp_set_post_terms($this->post_id, $chans, 'channel', true);
        }
    }


    /**
     * @param $unique_id
     *
     * @return int
     */
    public function existing_post_id($unique_id)
    {
        $this->unique_id = $unique_id;

        $existing_posts  = get_posts([
            'post_type'   => $this::POST_TYPE,
            'post_status' => 'any',
            'meta_key'    => 'unique_id',
            'meta_value'  => $this->unique_id,
        ]);

        $post_id = 0;

        if ($existing_posts && sizeof($existing_posts)) {
            $post_id = $existing_posts[0]->ID;
            papi_update_field($post_id, 'touched', time());
        }

        return $post_id;
    }

    private function set_meta($post_id)
    {
        papi_set_page_type_id($post_id, $this::PAGE_TYPE);

        papi_update_field($post_id, 'touched', time());

        papi_update_field($post_id, 'checksum', $this->get_checksum());

        papi_update_field($post_id, 'unique_id', $this->unique_id);

        //preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $this->content, $matches);
        //if ($matches && sizeof($matches) >= 2 && ! $this->thumbnail) {
        //    $this->thumbnail = urlencode( $matches[1] );
        //}

        if ($this->thumbnail) {
            papi_update_field($post_id, 'external_image', $this->thumbnail);
        }

        papi_update_field($post_id, 'source_id', $this->feed_post->ID);
        papi_update_field($post_id, 'source_title', $this->feed_post->post_title);
        papi_update_field($post_id, 'link', $this->link);
    }

    private function update_post($post_id)
    {
        wp_defer_term_counting(true);
        $this->post_data['ID'] = $post_id;
        wp_update_post($this->post_data);

        return $post_id;
    }

    private function insert_post()
    {
        wp_defer_term_counting(true);
        $post_id = wp_insert_post($this->post_data);
        if (! $post_id) {
            throw new \Exception('Newsflow unable to persist feed item as WordPress post');
        }

        return $post_id;
    }

    public function get_post_id()
    {
        return $this->post_id;
    }

    public function replacements()
    {
        $this->replacement_posts = papi_get_field($this->feed_post->ID, 'replace');
        if ($this->replacement_posts) {
            foreach ($this->replacement_posts as $replace_post) {
                $pattern = '/' . papi_get_field($replace_post->ID, 'pattern') . '/i';
                $replacement = papi_get_field($replace_post->ID, 'replacement');
                if ($pattern && $replacement) {
                    $this->title = preg_replace($pattern, $replacement, $this->title);
                    $this->link = preg_replace($pattern, $replacement, $this->link);
                    $this->content = preg_replace($pattern, $replacement, $this->content);
                    $this->date = preg_replace($pattern, $replacement, $this->date);
                    $this->thumbnail = preg_replace($pattern, $replacement, $this->thumbnail);
                }
            }
        }
    }

    public function matches()
    {
        $this->matching_posts = papi_get_field($this->feed_post->ID, 'matching');
        $old_matches = get_post_status($this->post_id) == 'publish' ? true : false;
        $matches     = $this->traverse_matches();
        if ($old_matches != $matches) {
            $this->is_item_dirty = true;
        }

        return $matches;
    }


    private function traverse_matches()
    {
        if ($this->matching_posts) {
            foreach ($this->matching_posts as $matching_post) {
                $contains = papi_get_field($matching_post->ID, 'contains');
                $subject  = $this->title . ' ' . $this->content . ' ';
                $subject .= utf8_encode($subject);
                $subject .= html_entity_decode($subject);
                if ($contains) {
                    foreach ($contains as $param) {
                        $pattern = '/' . $param['word'] . '/s';
                        if (! $param['case_sensitive']) {
                            $pattern .= 'i';
                        }
                        preg_match($pattern, $subject, $m);
                        if (sizeof($m)) {
                            $this->set_taxonomies($matching_post->ID);
                            return true;
                        }
                    }
                }
            }
        } else {
            return true;
        }

        return false;
    }

    public function display()
    {
        ?>
        <table>
            <tr>
                <td>
                    <img src="<?=$this->thumbnail?>" /><br/>
                </td>
                <td>
                    <h2><a href="<?=$this->link?>" target="_blank"><?=$this->title?></a></h2>
                    <i><?=$this->date?></i> <i>ID=<?=$this->post_id?></i> <i><?=get_post_status($this->post_id)?></i> <i><?=$this->is_dirty() ? 'DIRTY' : 'clean' ?></i>
                    <p>
                        <?=$this->content?>
                    </p>
                </td>
            </tr>
        </table>
        <?php

    }
}
