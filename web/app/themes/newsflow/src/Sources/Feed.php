<?php
namespace Newsflow\Sources;

class Feed extends Base implements SourceInterface
{

    public function fetch()
    {
        try {
            $client = new \Zend\Http\Client(null, [
                'timeout' => $this->timeout
            ]);
            \Zend\Feed\Reader\Reader::setHttpClient($client);
            $reader = \Zend\Feed\Reader\Reader::import($this->url);
        } catch (\Exception $ex) {
            $this->report_error($ex->getMessage());

            return;
        }

        $this->report_ok();

        /*$image = $reader->getImage();
        if ($image && isset( $image['uri'] ) && isset( $image['link'] )) {
            $url = $image['uri'];
            if ( ! strstr( $url, '//' )) {
                $url = rtrim( $image['link'], '/' ) . $url;
            }
            $this->set_thumbnail( $url );
        }*/

        foreach ($reader as $item) {
            $escaper = new \Zend\Escaper\Escaper('utf-8');

            $gmt_date = date_i18n('Y-m-d H:i:s');
            try {
                if ($item->getDateModified()) {
                    $gmt_date = $item->getDateModified()->format('Y-m-d H:i:s');
                }
            } catch (\Exception $ex) {
            }
            $date = \get_date_from_gmt($gmt_date);

            $thumbnail = null;
            preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $item->getDescription(), $matches);
            if ($matches && sizeof($matches) >= 2 && ! property_exists($this, 'thumbnail')) {
                $thumbnail = $matches[1];
            }

            $news_item = $this->add_news_item(
                $item->getId(),
                $escaper->escapeHtml($item->getTitle()),
                wp_kses_post($item->getDescription()),
                $escaper->escapeHtml($item->getLink()),
                $date,
                $thumbnail
            );
            $news_item->display();
        }
    }
}
