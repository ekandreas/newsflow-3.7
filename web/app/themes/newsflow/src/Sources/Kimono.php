<?php
namespace Newsflow\Sources;

class Kimono extends Base implements SourceInterface
{

    public function fetch()
    {
        $pattern_title      = papi_get_field($this->post->ID, 'pattern_title');
        $pattern_content    = papi_get_field($this->post->ID, 'pattern_content');
        $pattern_date       = papi_get_field($this->post->ID, 'pattern_date');
        $pattern_link       = papi_get_field($this->post->ID, 'pattern_link');
        $pattern_categories = papi_get_field($this->post->ID, 'pattern_categories');
        $pattern_image      = papi_get_field($this->post->ID, 'pattern_image');

        $url = 'https://www.kimonolabs.com/api/' . papi_get_field($this->post->ID, 'api') . '?apikey=' . papi_get_field($this->post->ID, 'key');

        $client = new \GuzzleHttp\Client();
        $res    = $client->request('GET', $url, ['timeout' => $this->timeout]);

        if ($res->getStatusCode() != 200) {
            $this->report_error($res->getReasonPhrase());

            return;
        }

        $this->report_ok();

        $object = $res->getBody();

        $data = json_decode($object->getContents(), true);



        foreach ($data['results']['collection1'] as $item) {
            $document = new \johnitvn\jsonquery\JsonDocument();

            $document->addValue('-', $item);

            $content = trim(Helper::strip_tags_content($document->getValue($pattern_content)));

            if (isset($pattern_date) && $pattern_date != 'null') {
                $date = $document->getValue($pattern_date);
                $date = !empty($date) && is_string($date) ? $date : date('Y-m-d H:i');
                $date = Helper::parseDate($date);
            } else {
                $date = date('Y-m-d H:i');
            }

            $id = $document->getValue($pattern_link);
            $title = $document->getValue($pattern_title);
            $link = $document->getValue($pattern_link);
            $image = $pattern_image!='null' ? $document->getValue($pattern_image) : '';
            $categories = $pattern_categories!='null'?$document->getValue($pattern_categories):'';

            $news_item = $this->add_news_item(
                $id,
                $title,
                $content,
                $link,
                $date,
                $image,
                $categories
            );

            $news_item->display();
        }
    }
}
