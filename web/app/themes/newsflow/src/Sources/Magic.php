<?php
namespace Newsflow\Sources;

class Magic extends base implements SourceInterface
{

    public function fetch()
    {
        $pattern_title      = papi_get_field($this->post->ID, 'pattern_title');
        $pattern_content    = papi_get_field($this->post->ID, 'pattern_content');
        $pattern_date       = papi_get_field($this->post->ID, 'pattern_date');
        $pattern_link       = papi_get_field($this->post->ID, 'pattern_link');
        $pattern_categories = papi_get_field($this->post->ID, 'pattern_categories');
        $pattern_image      = papi_get_field($this->post->ID, 'pattern_image');

        $magic_user = papi_get_field('magic_user');
        $magic_key = papi_get_field('magic_key');

        $client = new \GuzzleHttp\Client();
        $res    = $client->request('GET',
            'https://api.import.io/store/connector/_magic?url=' .
                $this->url . '&js=false&_user=' .
                $magic_user . '&_apikey=' .
                $magic_key,
            ['timeout' => $this->timeout]);

        if ($res->getStatusCode() != 200) {
            $this->report_error($res->getReasonPhrase());
            return;
        }

        $this->report_ok();

        $object = $res->getBody();

        $tables = json_decode($object->getContents())->tables;

        foreach ($tables[0]->results as $item) {
            $item = get_object_vars($item);

            $date = $item[$pattern_date] ? $item[$pattern_date] : date('Y-m-d H:i');
            $date = Helper::parseDate($date);

            $this->add_news_item(
                $item[$pattern_link],
                $item[$pattern_title],
                $item[$pattern_content],
                $item[$pattern_link],
                $date,
                $item[$pattern_image],
                $item[$pattern_categories]
            );
        }

        // https://api.import.io/store/connector/_magic?url=http%3A%2F%2Fwww.mynewsdesk.com%2Fse%2Fmimer%2Flatest_news&js=false&_user=2f108ee2-9ea8-4ea2-88f3-12d64e960df1&_apikey=2f108ee29ea84ea288f312d64e960df11d31ddb0ea4cfe7aa19b014e8c4f9b26a0e53f05bd7ba984e2cd7dd56d72ae7470ee10099124e77dcf8a92120bead19a628385c3e3080a81da3389dafd5d6c80
    }
}
