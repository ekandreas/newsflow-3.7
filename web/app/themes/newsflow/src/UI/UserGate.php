<?php
namespace Newsflow\UI;

class UserGate
{
    public static function template_redirect()
    {
        if (get_current_blog_id()==1 && is_user_logged_in() && !is_super_admin() && !strpos($_SERVER['REQUEST_URI'], 'usergate/')) {
            wp_redirect(home_url('/usergate/'));
            exit();
        }
    }

    public static function query_vars($vars)
    {
        $vars[] = "usergate";
        return $vars;
    }

    public static function template_include($template_path)
    {
        global $post;

        if (get_query_var('usergate')) {
            $template_path = get_template_directory() . '/usergate.php';
        }

        return $template_path;
    }

    public static function currentUserSites($user_id=null)
    {
        if (!$user_id) {
            $user_id = get_current_user_id();
        }
        $blogs = array_values(get_blogs_of_user($user_id));
        foreach ($blogs as $key => $blog) {
            if ($blog->userblog_id==1) {
                unset($blogs[$key]);
            }
        }
        return $blogs;
    }

    public static function getUserUrl()
    {
        $result = [
            'status'=>'error',
            'data' => [],
            'message'=>__('Nonce error!', 'newsflow'),
        ];

        $nonce = isset($_REQUEST['nonce']) ? $_REQUEST['nonce'] : '';
        if ($nonce && wp_verify_nonce($nonce, 'gatekeeper')) {
            $current_user = wp_get_current_user();

            $meta = get_user_meta($current_user->ID);
            if (isset($meta['workplace'])) {
                $workplace = $meta['workplace'][0];
            } else {
                $workplace = $current_user->display_name;
            }

            $blogs = get_blogs_of_user($current_user->ID);
            foreach ($blogs as $key => $blog) {
                if ($blog->userblog_id==1) {
                    unset($blogs[$key]);
                }
            }

            if (!sizeof($blogs)) {
                $workplace_slug = '/'.sanitize_title($workplace);
                $current_site = get_current_site();
                $domain = $current_site->domain;

                if (domain_exists($domain, $workplace_slug)) {
                    $number=1;
                    $new_workplace_slug = $workplace_slug . '-' . $number;
                    while (domain_exists($domain, $new_workplace_slug)) {
                        $number=1;
                        $new_workplace_slug = $workplace_slug . '-' . $number;
                    }
                    $workplace_slug = $new_workplace_slug;
                }

                $new_blog_id = wpmu_create_blog($domain, $workplace_slug, $workplace, 1);
                add_user_to_blog($new_blog_id, get_current_user_id(), 'subscriber');
                update_blog_option($new_blog_id, 'siteurl', WP_SITEURL);
                update_blog_option($new_blog_id, 'stylesheet', 'newsflow');
                update_blog_option($new_blog_id, 'template', 'newsflow');

                $result['status']='success';
                $result['data']=$workplace_slug;
                $result['message']='';
            } elseif (sizeof($blogs)==1) {
                $result['status']='success';
                $result['data']=reset($blogs)->path;
                $result['message']='';
            } else {
                $result['status']='success';
                foreach (\Newsflow\UI\UserGate::currentUserSites() as $key => $site) {
                    $row = [
                        'url' => $site->path,
                        'name' => $site->blogname,
                    ];
                    $result['data'][]=$row;
                }
                $result['message']='';
            }
        }

        wp_send_json($result);
    }
}
