<?php

return [
    'title' => 'Source System',
    papi_property([
        'title' => 'Size',
        'slug'  => 'size',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'Match',
        'slug'  => 'match',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'Checksum',
        'slug'  => 'checksum',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'Load Time',
        'slug'  => 'load_time',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'Fetched',
        'description' => 'Timestamp',
        'slug'  => 'fetched',
        'type'  => 'number',
        'default' => 0,
    ]),
    papi_property([
        'title' => 'Updated',
        'description' => 'Timestamp',
        'slug'  => 'updated',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'Code',
        'slug'  => 'code',
        'type'  => 'string',
    ]),
    papi_property([
        'title' => 'Status',
        'slug'  => 'status',
        'type'  => 'string',
    ]),
    papi_property([
        'title' => 'Error Count',
        'slug'  => 'error_count',
        'type'  => 'number',
    ]),
    papi_property([
        'title' => 'External Image Thumbnail',
        'slug'  => 'external_image',
        'type'  => 'string',
    ]),
    papi_property([
        'title' => 'Timeout',
        'slug'  => 'timeout',
        'type'  => 'number',
        'default' => 10,
    ]),
    papi_property([
        'title' => 'Time To Live (TTL)',
        'slug'  => 'ttl',
        'type'  => 'number',
        'default' => 3600,
    ]),
];
