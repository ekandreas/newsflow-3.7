<?php

return [
    'title' => 'Item Match Filters',
    papi_property([
        'title'    => 'Match Filter',
        'slug'     => 'matching',
        'type'     => 'relationship',
        'settings' => [
            'placeholder' => 'Get all feed items without matching filter',
            'post_type'   => 'config',
            'query'       => [
                'meta_key'   => PAPI_PAGE_TYPE_KEY,
                'meta_value' => 'match',
            ]
        ],
    ]),
    papi_property([
        'title' => 'Out of bounds Matched',
        'slug'  => 'out_of_bounds_matched',
        'type'  => 'number',
        'description' => 'Maximum age of untouched and matched items',
        'default' => 2678400,
    ]),
    papi_property([
        'title' => 'Out of bounds Unmatched',
        'slug'  => 'out_of_bounds_unmatched',
        'type'  => 'number',
        'description' => 'Maximum age of untouched and unmatched items',
        'default' => 86400,
    ]),
    papi_property([
        'title' => 'Delete saved items',
        'slug'  => 'delete',
        'type'  => 'bool',
        'description' => 'Delete all old news items fetched with this source',
        'before_html' => 'Click this to delete all items at update post. It will reset after work done.',
    ]),
];
