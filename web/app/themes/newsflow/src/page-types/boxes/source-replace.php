<?php

return [
    'title' => 'Item Replace data',
    papi_property([
        'title'    => 'Replace Data',
        'slug'     => 'replace',
        'type'     => 'relationship',
        'settings' => [
            'placeholder' => 'Get all data without replacements',
            'post_type'   => 'config',
            'query'       => [
                'meta_key'   => PAPI_PAGE_TYPE_KEY,
                'meta_value' => 'replace',
            ]
        ],
    ]),
];
