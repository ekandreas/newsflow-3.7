<?php

return [
    'title' => 'Data Output Pattern',
    papi_property([
        'title' => 'Title',
        'slug'  => 'pattern_title',
        'type'  => 'string',
        'default' => '/title/text',
    ]),
    papi_property([
        'title' => 'Content',
        'slug'  => 'pattern_content',
        'type'  => 'string',
        'default' => '/content/text',
    ]),
    papi_property([
        'title' => 'Link',
        'slug'  => 'pattern_link',
        'type'  => 'string',
        'default' => '/title/href',
    ]),
    papi_property([
        'title' => 'Date',
        'slug'  => 'pattern_date',
        'type'  => 'string',
        'default' => '',
    ]),
    papi_property([
        'title' => 'Image',
        'slug'  => 'pattern_image',
        'type'  => 'string',
        'default' => '',
    ]),
    papi_property([
        'title' => 'Categories',
        'slug'  => 'pattern_categories',
        'type'  => 'string',
        'default' => '',
    ]),
];
