<?php
namespace Newsflow\Server\PageType;

class magic extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'source',
            'name'        => 'Magic',
            'description' => 'Simple Import.io importer with magic pattern',
        ];
    }

    public function remove()
    {
        return [
            'editor',
        ];
    }

    public function register()
    {
        $this->box('Import.io',
            [
                papi_property([
                    'title' => 'Url',
                    'slug'  => 'url',
                    'type'  => 'string',
                ]),
                papi_property([
                    'title' => 'Timeout',
                    'slug'  => 'timeout',
                    'type'  => 'number',
                    'default' => 10,
                ]),
            ]
        );

        $this->box('boxes/source-pattern.php');
        $this->box('boxes/source-match.php');
        $this->box('boxes/source-replace.php');
        $this->box('boxes/source-system.php');
    }
}
