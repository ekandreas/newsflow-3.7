<?php
namespace Newsflow\Server\PageType;

class match extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'config',
            'name'        => 'Match',
            'description' => 'Newsflow Match filter',
        ];
    }

    public function remove()
    {
        return [
            'editor',
        ];
    }

    public function register()
    {
        $this->box('Filter', [
            papi_property([
                'title' => 'Contains',
                'slug'  => 'contains',
                'type'  => 'repeater',
                'description' => 'Regular expression performed on the title and content of news items',
                'settings' => [
                    'items' => [
                        papi_property([
                            'title' => 'Match',
                            'slug' => 'word',
                            'type' => 'string',
                        ]),
                        papi_property([
                            'title' => 'Case Sensitive',
                            'slug' => 'case_sensitive',
                            'type' => 'bool',
                            'description' =>'Will set regexp in Sensitive mode (!i)',
                        ]),
                    ]
                ]
            ]),
        ]);
    }
}
