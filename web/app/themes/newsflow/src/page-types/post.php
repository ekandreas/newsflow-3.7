<?php
namespace Newsflow\Server\PageType;

class post extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'post',
            'name'        => 'Newsflow Post',
        ];
    }

    public function register()
    {
        $this->box('Feed', [
            papi_property([
                'title' => 'Checksum',
                'slug'  => 'checksum',
                'type'  => 'number',
            ]),
            papi_property([
                'title' => 'Id',
                'slug'  => 'unique_id',
                'type'  => 'string',
            ]),
            papi_property([
                'title' => 'Source',
                'slug'  => 'source_id',
                'type'  => 'number',
            ]),
            papi_property([
                'title' => 'Source Title',
                'slug'  => 'source_title',
                'type'  => 'string',
            ]),
        ]);

        $this->box('Extended', [
            papi_property([
                'title' => 'Link',
                'slug'  => 'link',
                'type'  => 'url',
            ]),
            papi_property([
                'title' => 'External Image Thumbnail',
                'slug'  => 'external_image',
                'type'  => 'url',
            ]),
            papi_property([
                'title' => 'Touched',
                'slug'  => 'touched',
                'type'  => 'number',
            ]),
        ]);
    }
}
