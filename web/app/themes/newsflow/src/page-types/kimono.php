<?php
namespace Newsflow\Server\PageType;

class kimono extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'source',
            'name'        => 'Kimono',
            'description' => 'Kimono API pattern',
        ];
    }

    public function remove()
    {
        return [
            'editor',
        ];
    }

    public function register()
    {
        $this->box('Kimono',
            [
                papi_property([
                    'title' => 'Api',
                    'slug'  => 'api',
                    'type'  => 'string',
                    'default' => 'dl4vbgjk',
                ]),
                papi_property([
                    'title' => 'Key',
                    'slug'  => 'key',
                    'type'  => 'string',
                    'default' => 'ZOori0XJ4hFLo2H0LB6vTq5T4CpUrJ9Z',
                ]),
                papi_property([
                    'title' => 'Timeout',
                    'slug'  => 'timeout',
                    'type'  => 'number',
                    'default' => 10,
                ]),
            ]
        );

        $this->box('boxes/source-pattern.php');
        $this->box('boxes/source-match.php');
        $this->box('boxes/source-replace.php');
        $this->box('boxes/source-system.php');
    }
}
