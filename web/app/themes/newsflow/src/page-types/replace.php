<?php
namespace Newsflow\Server\PageType;

class replace extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'config',
            'name'        => 'Replace',
            'description' => 'Newsflow Replace filter',
        ];
    }

    public function remove()
    {
        return [
            'editor',
        ];
    }

    public function register()
    {
        $this->box('Replace', [
            papi_property([
                'title' => 'Pattern',
                'slug'  => 'pattern',
                'type'  => 'string',
                'description' => 'Regular expression to find what to replace',
            ]),
            papi_property([
                'title' => 'Replacement',
                'slug'  => 'replacement',
                'type'  => 'string',
                'description' => 'Replace value',
            ]),
        ]);
    }
}
