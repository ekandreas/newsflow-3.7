<?php
namespace Newsflow\Server\PageType;

class feed extends \Papi_Page_Type
{

    public function meta()
    {
        return [
            'post_type'   => 'source',
            'name'        => 'Feed',
            'description' => 'Newsflow RSS feed',
        ];
    }

    public function remove()
    {
        return [
            'editor',
        ];
    }

    public function register()
    {
        $this->box('Feed',
            [
                papi_property([
                    'title' => 'Url',
                    'slug'  => 'url',
                    'type'  => 'string',
                ]),

            ]
        );

        $this->box('boxes/source-match.php');
        $this->box('boxes/source-replace.php');
        $this->box('boxes/source-system.php');
    }
}
