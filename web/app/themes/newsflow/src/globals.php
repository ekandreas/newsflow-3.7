<?php
include(ABSPATH.'../../vendor/johnbillion/extended-taxos/extended-taxos.php');

/**
 * Block external WordPress API request
 */
function wp_api_block_request($pre, $args, $url)
{
    if (strpos($url, 'api.wordpress.org')) {
        return true;
    } else {
        return $pre;
    }
}
add_filter('pre_http_request', 'wp_api_block_request', 10, 3);

add_theme_support('post-thumbnails');

/**
 * Global assets functions.
 */
function assets($file)
{
    return Newsflow\UI\Assets::get($file);
}

/**
 * Global assets functions.
 */
function asset($file)
{
    return Newsflow\UI\Assets::get($file);
}
