<?php
    $current_user = wp_get_current_user();
?>
<div class="ui attached blue inverted top main menu">
  <div class="ui text container">
    <a href="/" class="active item">Newsflow</a>
    <div class="ui right floated dropdown item">

      {{ $current_user->display_name }} <i class="dropdown icon"></i>

      <div class="menu">

        @if(current_user_can("edit_posts"))
            @if($edit_url=get_edit_post_link())
                <a class="item" href="{{ $edit_url }}">
                    <i class="fa fa-edit icon"></i>&nbsp;
                    <?=__('Edit content','intra')?>
                </a>
            @endif
            <a class="item" href="{{ admin_url('/') }}">
                <i class="fa fa-gear icon"></i>&nbsp;
                <?=__('WP-Admin','intra')?>
            </a>
            <div class="divider"></div>
        @endif
        <a class="item" href="{{ wp_logout_url('/') }}">
            <i class="fa fa-power-off icon"></i>&nbsp;
            <?=__('Logout','intra')?>
        </a>

      </div>
    </div>
  </div>
</div>
