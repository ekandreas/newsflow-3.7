<h1 class="ui header">
	<i class="settings blue icon"></i>
	<div class="content">
		{{ get_bloginfo('title') }}
		<div class="sub header"><?=__('Dashboard', 'newsflow')?></div>
	</div>
</h1>

<div class="ui statistic segment">
  <div class="value">
    {{ wp_count_posts('source')->publish }}
  </div>
  <div class="label">
    Sources
  </div>
</div>

<div class="ui statistic segment">
  <div class="value">
    {{ wp_count_posts('post')->publish }}
  </div>
  <div class="label">
    Posts
  </div>
</div>