<div class="ui attached blue inverted top main menu">
  <div class="ui text container">
    <a href="{{ get_bloginfo('url') }}" class="active item">Newsflow</a>
    <a href="#" onclick="$('#loginModal').modal('show');" class="ui right floated item">
      Login&nbsp;<i class="fa fa-sign-in"></i>
    </a>
  </div>
</div>
