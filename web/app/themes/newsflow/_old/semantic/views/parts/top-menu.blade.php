@if(is_user_logged_in()) 
	@include('views.parts.top-menu-auth')
@else
	@include('views.parts.top-menu-unauth')
@endif