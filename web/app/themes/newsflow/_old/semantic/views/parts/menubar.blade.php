<div class="ui secondary vertical menu">

	<a class="active item" href="{{ get_bloginfo('url') }}">
		{{ get_bloginfo('title') }}
	</a>

	@if(is_user_logged_in()) 
		<a class="item" href="./channel/1">
			Channel 1
		</a>
		<a class="item" href="./channel/2">
			Channel 2
		</a>
		<a class="item" href="./channel/3">
			Channel 3
		</a>
	@else
		<a class="item" href="./nothing">
			Nothing
		</a>
	@endif

</div>
