@if(is_user_logged_in()) 
	<a href="./sources/add"><i class="fa fa-plus"></i>&nbsp;<?=__('Add new source','newsflow')?></a>
@else
	@include('views.parts.wip')

	<div class="ui hidden divider"></div>

	<div class="ui statistic segment raised">
	  <div class="value">
	    5,550
	  </div>
	  <div class="label">
	    News fetched
	  </div>
	</div>
@endif

<div class="ui hidden divider"></div>
<div class="ui hidden divider"></div>

