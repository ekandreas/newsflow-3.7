<div class="ui small modal login" id="loginModal">
  <div class="header"><?=__('Login','newsflow')?></div>
  <div class="content">
    
    <p>
		{!! papi_get_option('message_login') !!}
    </p>
    
    <?php do_action( 'wordpress_social_login' ); ?>

    <div v-show="success" class="ui positive message" role="alert">
        @{{{ success }}}
    </div>

    <div v-show="warning" class="ui warning message" role="alert">
        @{{{ warning }}}
    </div>

    <div v-show="error" class="ui negative message" role="alert">
        @{{{ error }}}
    </div>
	
	<form class="ui form" method="post" action="#">
	  <div class="field">
	    <label><?=__('Email Address','newsflow')?></label>
	    <input v-model="email" v-on:keyup.enter="login" type="text" name="email" placeholder="john@doe.com">
	  </div>
	  <div class="field">
	    <label><?=__('Password','newsflow')?></label>
	    <input v-model="password" v-on:keyup.enter="login" type="password" name="password">
	  </div>
	  <div class="field">
	    <div class="ui checkbox">
	      <input v-model="remember" name="remember" type="checkbox" tabindex="0" class="hidden">
	      <label><?=__('Remember login','newsflow')?></label>
	    </div>
	  </div>
	  <button v-on:click="login" class="ui primary button" type="button">Login</button>
	  <button class="ui basic button" onclick="jQuery('#forgotModal').modal('show'); jQuery('#loginModal').modal('hide'); return false;"><i class="fa fa-question"></i>&nbsp;<?=__('Password lost','newsflow')?></button>
	</form>
  </div>
  <div class="actions">
    <div class="ui green basic button"><i class="fa fa-slack"></i>&nbsp;<?=__('Slack','newsflow')?></div>
    <div class="ui orange basic button" onclick="jQuery('#registerModal').modal('show'); jQuery('#loginModal').modal('hide'); return false;"><i class="fa fa-user-plus"></i>&nbsp;<?=__('New Account','newsflow')?></div>
    <div class="ui black basic cancel button"><i class="fa fa-times"></i>&nbsp;<?=__('Close','newsflow')?></div>
  </div>
</div>
