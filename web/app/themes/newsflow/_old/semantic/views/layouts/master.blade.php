<!doctype html>
<html lang="en">
@include('views.parts.head')
<body {{ body_class() }}>
  @include('views.parts.google-analytics')

  @include('views.parts.top-menu')

  <div class="ui hidden divider"></div>
  <div class="ui hidden divider"></div>

  <div class="ui container">

    <div class="equal width stackable ui grid">

      <div class="three wide column">
        <div class="ui">
          @include('views.parts.menubar')
        </div>
      </div>

      <div class="nine wide column">
        @yield('main')
      </div>

      <div class="four wide column center aligned">
          @include('views.parts.sidebar')
      </div>

    </div>

    @include('views.parts.footer')

    @if(!is_user_logged_in())
      @include('views.modals.login')
      @include('views.modals.register')
      @include('views.modals.forgot')
    @endif

  </body>
  @include('views.parts.scripts')
  </html>
