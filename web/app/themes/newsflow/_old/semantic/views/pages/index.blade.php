@extends('views.layouts.master2')

@section('main')

	<div class="ui">
		@if(is_user_logged_in()) 
			@include('views.parts.dashboard')
		@else
			@include('views.parts.newsflow-header')
			@include('views.parts.lorem')
			@include('views.parts.lorem')
			@include('views.parts.lorem')
			@include('views.parts.lorem')
		@endif


	</div>

    <div class="ui hidden divider"></div>
    <div class="ui hidden divider"></div>

@endsection

@section('scripts')
	<script type="text/javascript">
    	jQuery(document).ready(function($){
    		//$('.dropdown').dropdown();
    	});
	</script>
@endsection