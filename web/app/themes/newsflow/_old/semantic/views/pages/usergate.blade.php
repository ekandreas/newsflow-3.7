@extends('views.layouts.master')

@section('main')

	<div class="ui" id="userGate">

		<?php 
            $current_user = wp_get_current_user();
        ?>

		<h1 class="ui header">
			<i class="fa fa-code-fork red icon"></i>
			<div class="content">
				{{ $current_user->display_name }}
				<div class="sub header"><?=__('Getting you to the right team', 'newsflow')?></div>
			</div>
		</h1>

		<p>
			<?=__('You are now logged in and you need to get to the right team site!', 'newsflow')?>
		</p>

	    <div v-show="success" class="ui positive message" role="alert">
	        @{{{ success }}}
	    </div>

		<div v-show="warning" class="ui message" role="alert">
			<div class="ui active small inline loader"></div>&nbsp;
			<?=__('Please wait while we get your team site...')?>
		</div>

	    <div v-show="error" class="ui negative message" role="alert">
	        @{{{ error }}}
	    </div>

		<div v-show="sites" class="ui relaxed divided list">
		    <div class="ui hidden divider"></div>
			<h2><?=__('Multiple teams', 'newsflow')?></h2>
			<p><?=__('Please select the team you want to work with!', 'newsflow')?></p>
			<div v-for="site in sites" class="item">
				<i class="large icon fa fa-sign-in"></i>
				<div class="content">
					<a href="@{{ site.url }}">
						<h3>@{{ site.name }}</h3>
					</a>
				</div>
			</div>
		</div>

	</div>

    <div class="ui hidden divider"></div>
    <div class="ui hidden divider"></div>

@endsection

@section('scripts')
	<script type="text/javascript">
    	jQuery(document).ready(function($){
    		//$('.dropdown').dropdown();
    	});
	</script>
@endsection