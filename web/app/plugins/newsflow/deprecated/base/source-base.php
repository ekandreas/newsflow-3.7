<?php
namespace newsflow\lib;

abstract class SourceBase
{
    const POST_TYPE = 'newsflow-source';

    private $is_source_dirty;
    private $source_checksum;

    private $load_time_start;

    public $matched_news;
    private $news;

    public $post;

    protected $timeout;
    protected $ttl;

    public function __construct($post)
    {
        $this->matched_news = [ ];
        $this->news         = [ ];

        if (is_numeric($post)) {
            $this->post = get_post($post);
        } else {
            $this->post = $post;
        }

        $this->url = papi_get_field($this->post->ID, 'url');

        $timeout = (int)papi_get_field($this->post->ID, 'timeout');
        $this->timeout = $timeout ? $timeout : 10;

        $ttl = (int)papi_get_field($this->post->ID, 'ttl');
        $this->ttl = $ttl ? $ttl : 3600;
    }

    abstract public function fetch();

    public function pre_fetch()
    {
        $this->is_source_dirty = false;
        $this->source_checksum = 0;
        $this->load_time_start = microtime(true);

        $this->mark_started();

        echo "<h1>" . $this->post->post_title . "</h1>";

        papi_update_field($this->post->ID, 'fetched', time());
    }

    public function post_fetch()
    {
        $load_time = round((microtime(true) - $this->load_time_start), 1);

        if ($this->is_source_dirty) {
            papi_update_field($this->post->ID, 'checksum', crc32($this->source_checksum));
            papi_update_field($this->post->ID, 'updated', time());
            //$this->remove_old_news_items();
        }

        $updated = papi_get_field($this->post->ID, 'updated');
        if (! $updated) {
            papi_update_field($this->post->ID, 'updated', time());
        }

        papi_update_field($this->post->ID, 'size', sizeof($this->news));
        papi_update_field($this->post->ID, 'match', sizeof($this->matched_news));
        papi_update_field($this->post->ID, 'load_time', $load_time);
        papi_delete_field($this->post->ID, 'error_count');

        $this->mark_done();
    }

    public function mark_started()
    {
        papi_update_field($this->post->ID, 'status', 'FETCHING');
    }

    private function mark_done()
    {
        papi_delete_field($this->post->ID, 'status');
    }

    public function inactivate()
    {
        papi_update_field($this->post->ID, 'status', 'INACTIVE');
    }

    public function is_touched()
    {
        $status = papi_get_field($this->post->ID, 'status');
        if ($status && strlen($status)) {
            return true;
        }

        return false;
    }

    public function remove_old_news_items()
    {
        $posts = get_posts([
            'post_type'      => 'newsflow-item',
            'posts_per_page' => - 1,
            'meta_query'     => array(
                'relation' => 'AND',
                array(
                    'key'   => PAPI_PAGE_TYPE_KEY,
                    'value' => 'news',
                ),
                array(
                    'key'   => 'source_id',
                    'value' => $this->post->ID,
                ),
            ),
        ]);

        $prevent_post_ids = [ ];
        foreach ($this->matched_news as $news) {
            $prevent_post_ids[] = $news->post_id;
        }

        foreach ($posts as $post) {
            if (! in_array($post->ID, $prevent_post_ids)) {
                //wp_trash_post( $post->ID );
            }
        }
    }

    public function increment_checksum($value)
    {
        $this->source_checksum += $value;
    }

    /**
     * @param      $id
     * @param      $title
     * @param      $content
     * @param      $link
     * @param      $date
     * @param null $thumbnail
     * @param null $categories
     *
     * @return News_Item
     */
    public function add_news_item($id, $title, $content, $link, $date, $thumbnail = null, $categories = null)
    {
        $news_item = new News_Item($id, $title, $content, $link, $date, $thumbnail);

        $news_item->feed_post = $this->post;

        $news_item->replacements();

        $matches = $news_item->matches();

        $news_item->persist($matches);

        if ($news_item->is_dirty()) {
            $this->is_source_dirty = true;
        }

        $this->increment_checksum($news_item->get_checksum());

        $this->news[] = $news_item;
        if ($matches) {
            $this->matched_news[] = $news_item;
        }

        return $news_item;
    }

    protected function report_error($message)
    {
        $error_count = (int) papi_get_field($this->post->ID, 'error_count');
        papi_update_field($this->post->ID, 'code', $message);
        $error_count ++;
        if ($error_count > 9) {
            papi_update_field($this->post->ID, 'status', 'ERROR');
        } else {
            papi_delete_field($this->post->ID, 'status');
        }
        papi_update_field($this->post->ID, 'error_count', $error_count);
    }

    protected function report_ok()
    {
        papi_update_field($this->post->ID, 'code', '200');
    }

    protected function set_thumbnail($url)
    {
        if (! has_post_thumbnail($this->post->ID)) {
            $file_headers = @get_headers($url);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                // doesn't exist
            } else {
                $media = wp_upload_bits($this->post->post_name . '.' . pathinfo($url, PATHINFO_EXTENSION), null,
                    file_get_contents($url));

                if (! $media['error']) {
                    $attachment_id = Utils::attach_image($media['file']);
                    set_post_thumbnail($this->post->ID, $attachment_id);
                }
            }
        }
    }

    public static function save_post($post_id)
    {
        if (wp_is_post_revision($post_id) ||
            wp_is_post_autosave($post_id) ||
            (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
            ! isset($_POST['post_type']) ||
            'newsflow-source' != $_POST['post_type']
        ) {
            return $post_id;
        }

        if (papi_get_field($post_id, 'delete')) {
            $posts = get_posts([
                'post_type'      => 'newsflow-item',
                'post_status'    => get_available_post_statuses('newsflow-item'),
                'meta_key'       => 'source_id',
                'meta_value'     => $post_id,
                'posts_per_page' => - 1,
            ]);
            if ($posts) {
                foreach ($posts as $post) {
                    wp_delete_post($post->ID, true);
                }
            }
            papi_delete_field($post_id, 'delete');
        }

        return $post_id;
    }
}
