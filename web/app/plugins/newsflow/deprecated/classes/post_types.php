<?php

namespace newsflow\lib;

class post_types
{
    public function __construct()
    {
        add_action('init', function () {

            register_post_status('unmatched', [
                'label'                     => _x('Unmatched', 'post'),
                'public'                    => false,
                'exclude_from_search'       => false,
                'show_in_admin_all_list'    => false,
                'show_in_admin_status_list' => true,
                'label_count'               => _n_noop('Unmatched <span class="count">(%s)</span>', 'Unmatched <span class="count">(%s)</span>'),
            ]);

            register_extended_post_type('newsflow-source', [

                'has_archive' => false,
                'show_ui' => true,
                'show_in_menu' => false,
                'show_in_feed' => false,
                'exclude_from_search' => true,
                'searchable' => false,
                'show_in_rest' => true,
                'admin_cols'   => array(
                    'status'        => array(
                        'title'    => 'Status',
                        'meta_key' => 'status',
                    ),
                    'updated'        => array(
                        'title'    => 'Updated',
                        'function' => function () {
                            echo human_time_diff(papi_get_field('updated')) . ' ago';
                        },
                    ),
                    'fetched'        => array(
                        'title'    => 'Fetched',
                        'function' => function () {
                            echo human_time_diff(papi_get_field('fetched')) . ' ago';
                        },
                    ),
                    'load_time' => array(
                        'title'    => 'Load Time (s)',
                        'function' => function () {
                            $load_time = papi_get_field('load_time');
                            $color = '#000';
                            if ($load_time > 20) {
                                $color = '#f00';
                            }
                            echo '<div style="color: ' . $color . ';text-align:right">' . $load_time . '</div>';
                        },
                    ),
                    'size' => array(
                        'title'    => 'Items',
                        'meta_key' => 'size',
                    ),
                    'match' => array(
                        'title'    => 'Match',
                        'meta_key' => 'match',
                    ),
                    'category' => array(
                        'taxonomy' => 'newsflow-category'
                    ),
                    'channel' => array(
                        'taxonomy' => 'newsflow-channel'
                    ),
                ),

            ], [
                'singular' => 'Source',
                'plural'   => 'Sources',
            ]);

            register_extended_post_type('newsflow-item', [

                'has_archive' => false,
                'show_ui' => true,
                'show_in_menu' => false,
                'show_in_feed' => false,
                'show_in_rest' => true,
                'admin_cols'   => array(
                    'content' => array(
                        'title'      => 'Content',
                        'function' => function () {
                            echo get_the_excerpt();
                        },
                    ),
                    'date' => array(
                        'title'      => 'Date',
                        'post_field' => 'post_date',
                        'default'    => 'DESC',
                    ),
                    'published' => array(
                        'title'      => 'Publication date',
                        'function' => function () {
                            echo get_the_time('Y-m-d H:i');
                        },
                    ),
                    'touched' => array(
                        'title'      => 'Touched',
                        'meta_key' => 'touched',
                        'date_format' => 'Y-m-d H:i'
                    ),
                    'source_title'        => array(
                        'title'    => 'Source',
                        'meta_key' => 'source_title',
                    ),
                    'category' => array(
                        'taxonomy' => 'newsflow-category'
                    ),
                    'channel' => array(
                        'taxonomy' => 'newsflow-channel'
                    ),
                ),

            ], [
                'singular' => 'Item',
                'plural'   => 'Items',
            ]);

            register_extended_post_type('newsflow-config', [

                'has_archive' => false,
                'show_ui' => true,
                'show_in_menu' => false,
                'show_in_feed' => false,

            ], [
                'singular' => 'Configuration',
                'plural'   => 'Configurations',
            ]);

        });
    }
}
