<?php
namespace newsflow\lib;

class papi
{
    public function __construct()
    {
        add_filter('papi/settings/directories', function ($directories) {
            $directories[] = dirname(dirname(dirname(__FILE__))) . '/page-types';
            return $directories;
        });
    }
}
