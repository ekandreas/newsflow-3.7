<?php

namespace newsflow\lib;

class inbox
{

    public function __construct()
    {
        add_action('wp_ajax_newsflow_opml_export', __NAMESPACE__ . '\Inbox::opml_export');
        add_action('wp_ajax_newsflow_clear_items', __NAMESPACE__ . '\Inbox::clear_items');
        add_action('wp_ajax_newsflow_out_of_bounds', 'newsflow\lib\Job::out_of_bounds');
    }

    public static function display()
    {
        ?>
        <div class="wrap">
            <h2>Newsflow</h2>

            <p>
                <a class="btn" target="_blank" href="<?= admin_url('admin-ajax.php') ?>?action=newsflow_opml_export">OPML Export</a>
            </p>

            <p>
                <a class="btn" target="_blank" href="<?= admin_url('admin-ajax.php') ?>?action=newsflow_clear_items">Delete all items (100)</a>
            </p>

            <p>
                <a class="btn" target="_blank" href="<?= admin_url('admin-ajax.php') ?>?action=newsflow_out_of_bounds">Out of bounds</a>
            </p>

        </div>
        <?php

    }

    public static function clear_items()
    {
        $posts = get_posts([
            'post_type'      => 'newsflow-item',
            'posts_per_page' => 500,
            'post_status'    => get_post_types('', 'names'),
        ]);

        foreach ($posts as $post) {
            wp_delete_post($post->ID);
        }


        $posts = get_posts([
            'post_type'      => 'newsflow-item',
            'posts_per_page' => 1,
            'post_status'    => get_post_types('', 'names'),
        ]);

        if (sizeof($posts)) {
            echo "Reloading...<br/>";
            ?>
            <script>
                window.location = '<?= admin_url('admin-ajax.php') ?>?action=newsflow_clear_items';
            </script>
            <?php

        }


        echo "Done, " . date_i18n('Y-m-d H:i:s') . ".";

        exit;
    }

    public static function opml_export()
    {
        header('Content-disposition: attachment; filename=newsflow.opml');
        header("Content-Type:text/xml");

        echo '<?xml version="1.0" encoding="UTF-8"?>';

        ?>
        <opml version="1.0">
            <head>
                <title>Newsflow</title>
            </head>
            <body>
            <outline title="Newsflow" text="Newsflow">
                <?php

                $posts = get_posts([
                    'post_type'      => 'newsflow-source',
                    'meta_key'       => PAPI_PAGE_TYPE_KEY,
                    'meta_value'     => 'feed',
                    'posts_per_page' => - 1
                ]);

        foreach ($posts as $key => $post) {
            $post = get_post($post->ID);
            $url  = htmlentities(papi_get_field($post->ID, 'url'));
            if ($key) {
                echo "\t\t\t";
            }
            echo '<outline text="' . $post->post_title . '" title="' . $post->post_title . '" type="rss" xmlUrl="' . $url . '" htmlUrl="' . $url . '"/>' . "\n";
        }

        ?>
            </outline>
            </body>
        </opml>

        <?php
        exit;
    }
}

new Inbox();
