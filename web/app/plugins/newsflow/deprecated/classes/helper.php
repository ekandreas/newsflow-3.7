<?php

namespace newsflow\lib;

class helper
{

    public static function parseDate($date_value)
    {
        $date = strtolower($date_value);
        $date = str_replace(',', '', $date);

        $date = str_replace('idag', date('Y-m-d'), $date);
        $date = str_replace('igår', date('Y-m-d', time()-86400), $date);

        $parsed_date = date_parse($date);
        $year        = $parsed_date['year'] ? $parsed_date['year'] : date('Y');
        $month       = $parsed_date['month'] ? $parsed_date['month'] : date('m');
        $day         = $parsed_date['day'] ? $parsed_date['day'] : date('d');
        $hour        = $parsed_date['hour'] ? $parsed_date['hour'] : date('H');
        $minute      = $parsed_date['minute'] ? $parsed_date['minute'] : date('i');
        $date    = date('Y-m-d H:i', strtotime("$year-$month-$day $hour:$minute"));

        return $date;
    }

    public static function strip_tags_content($text, $tags = '', $invert = false)
    {
        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) and count($tags) > 0) {
            if ($invert == false) {
                return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == false) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }
}
