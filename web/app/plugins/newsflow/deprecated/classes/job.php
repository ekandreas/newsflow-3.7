<?php

namespace newsflow\lib;

class job
{
    public function __construct()
    {
        add_action('wp_ajax_convert', __NAMESPACE__ . '\Job::convert');
        add_action('wp_ajax_newsflow-run', __NAMESPACE__ . '\Job::ajax_run');
        add_action('newsflow/job', __NAMESPACE__ . '\Job::run');
        add_filter('cron_schedules', __NAMESPACE__ . '\Job::add_new_intervals');
        add_action('admin_init', __NAMESPACE__ . '\Job::add_job');

        add_action('wp_ajax_fix', __NAMESPACE__ . '\Job::fix');
    }

    public static function fix()
    {
        $posts = get_posts([
            'post_type'      => 'newsflow-setting',
            'posts_per_page' => - 1,
        ]);

        if ($posts) {
            foreach ($posts as $post) {
                $post_data = [
                    'ID' => $post->ID,
                    'post_type' => 'newsflow-config'
                ];

                wp_update_post($post_data);
            }
        }

        exit;
    }

    public static function run()
    {
        Job::blocked_fetch();

        $loader = new Loader();
        $loader->load_sources();
        $loader->fetch();

        Job::out_of_bounds();
    }

    public static function ajax_run()
    {
        Job::run();
        die;
    }

    public static function convert()
    {
        $posts = get_posts([
            'post_type'      => 'newsflow-entity',
            'posts_per_page' => - 1,
        ]);

        if ($posts) {
            foreach ($posts as $post) {
                $post_data = [
                    'ID' => $post->ID,
                    'post_type' => 'newsflow-setting'
                ];

                wp_update_post($post_data);
            }
        }

        exit;
    }

    public static function add_new_intervals($schedules)
    {
        $schedules['minute'] = array(
            'interval' => 60,
            'display'  => __('Minute')
        );

        return $schedules;
    }

    public static function add_job()
    {
        if (! wp_next_scheduled('newsflow/job')) {
            wp_schedule_event(current_time('timestamp', true), 'minute', 'newsflow/job');
        }
    }


    public static function out_of_bounds()
    {

        //echo "<h1>Out of bounds</h1>";

        $posts = get_posts([
            'post_type'      => 'newsflow-item',
            'posts_per_page' => 100,
            'meta_key'       => 'touched',
            'meta_compare'   => '<',
            'meta_type'      => 'NUMERIC',
            'meta_value'     => time() - 86400,
            'post_status'    => 'any',
        ]);

        foreach ($posts as $key => $post) {
            $source = get_post(papi_get_field($post->ID, 'source_id'));
            $out_of_bounds_matched = (int)papi_get_field($source->ID, 'out_of_bounds_matched');
            $out_of_bounds_unmatched = (int)papi_get_field($source->ID, 'out_of_bounds_unmatched');
            $touched = papi_get_field($post->ID, 'touched');

            $current_out_of_bounds = time() - $touched;

            $matched = $post->post_status == 'publish' ? true : false;

            $delete = false;

            //echo "<span";

            if ($matched && $current_out_of_bounds > $out_of_bounds_matched) {
                //echo ' style="text-decoration:line-through"';
                $delete = true;
            }

            if (!$matched && $current_out_of_bounds > $out_of_bounds_unmatched) {
                //echo ' style="text-decoration:line-through"';
                $delete = true;
            }

/*
            echo ">";

            if( $matched ) echo "MATCHED: ";

            echo $key + 1 . '. ';
            echo $post->post_title . ', ';
            echo date_i18n( 'Y-m-d H:i', $touched );
            echo ', ';
            echo $source->post_title;
            echo ', ';
            echo $out_of_bounds_matched;
            echo ', ';
            echo $out_of_bounds_unmatched;
            echo ', ';
            echo $current_out_of_bounds;
            echo ', ';
            echo "<br/>";

            echo '</span>';
*/
            
            if ($delete) {
                wp_delete_post($post->ID);
            } else {
                papi_update_field($post->ID, 'touched', time());
            }
        }
    }

    public static function blocked_fetch()
    {

        //echo "<h1>Blocked fetch</h1>";

        $posts = get_posts([
            'post_type'      => 'newsflow-source',
            'posts_per_page' => 500,
            'meta_query' => [
                [
                    'key' => 'status',
                    'value' => 'FETCHING',
                    'compare' => '=',
                ],
                [
                    'key' => 'fetched',
                    'value' => time()-300,
                    'compare' => '<',
                    'type' => 'NUMERIC',
                ]
            ]
        ]);

        foreach ($posts as $key => $post) {
            $fetched = papi_get_field($post->ID, 'fetched');

            //echo $key + 1 . '. ';
            //echo $post->post_title . ', ';
            //echo date_i18n( 'Y-m-d H:i', $fetched );
            //echo "<br/>";

            papi_delete_field($post->ID, 'status');
        }
    }
}
