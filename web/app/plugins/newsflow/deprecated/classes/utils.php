<?php

namespace newsflow\lib;

class utils
{

    public static function debug_object($var)
    {
        if (WP_DEBUG) {
            echo '<pre>';
            var_dump($var);
            echo '</pre>';
        }
    }

    public static function get_attachment_id_from_src($image_src)
    {
        global $wpdb;
        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
        $id    = (int)$wpdb->get_var($query);

        return $id;
    }

    public static function attach_image($file_path)
    {
        require_once(ABSPATH . '/wp-admin/includes/media.php');
        require_once(ABSPATH . '/wp-admin/includes/file.php');
        require_once(ABSPATH . '/wp-admin/includes/image.php');

        $file_name = basename($file_path);
        $file_type = wp_check_filetype($file_name);

        $attachment = array(
            'post_mime_type' => $file_type['type'],
            'post_title'     => preg_replace('/\.[^.]+$/', '', basename($file_path)),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        $attach_id   = wp_insert_attachment($attachment, $file_path);
        $attach_data = wp_generate_attachment_metadata($attach_id, $file_path);
        wp_update_attachment_metadata($attach_id, $attach_data);

        return $attach_id;
    }
}
