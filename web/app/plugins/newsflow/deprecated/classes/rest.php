<?php

namespace newsflow\lib;

class rest
{
    public function __construct()
    {
        add_action('rest_api_init', __NAMESPACE__ . '\Rest::register_item');
    }

    public static function register_item()
    {
        \register_api_field('newsflow-item',
            'source',
            array(
                'get_callback'    => __NAMESPACE__ . '\Rest::get_item_source_name',
                'update_callback' => null,
                'schema'          => null,
            )
        );

        \register_api_field('newsflow-item',
            'external_link',
            array(
                'get_callback'    => __NAMESPACE__ . '\Rest::get_item_external_link',
                'update_callback' => null,
                'schema'          => null,
            )
        );

        \register_api_field('newsflow-item',
            'external_image',
            array(
                'get_callback'    => __NAMESPACE__ . '\Rest::get_item_external_image',
                'update_callback' => null,
                'schema'          => null,
            )
        );

        \register_api_field('newsflow-item',
            'post_content',
            array(
                'get_callback'    => __NAMESPACE__ . '\Rest::get_item_post_content',
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

    /**
     * Get the value of the "starship" field
     *
     * @param array $object Details of current post.
     * @param string $field_name Name of field.
     * @param WP_REST_Request $request Current request
     *
     * @return mixed
     */
    public static function get_item_source_name($object, $field_name, $request)
    {
        $source_id = \papi_get_field($object['id'], 'source_id');
        return get_the_title($source_id);
    }

    public static function get_item_external_link($object, $field_name, $request)
    {
        return \papi_get_field($object['id' ], 'link');
    }

    public static function get_item_external_image($object, $field_name, $request)
    {
        return \papi_get_field($object['id' ], 'external_image');
    }

    public static function get_item_post_content($object, $field_name, $request)
    {
        return strip_tags(get_the_content($object['id']));
    }
}

new Rest();
