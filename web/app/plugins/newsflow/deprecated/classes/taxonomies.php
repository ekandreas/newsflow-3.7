<?php

namespace newsflow\lib;

class taxonomies
{
    public function __construct()
    {
        add_action('init', function () {

            if (class_exists('Extended_Taxonomy')) {
                \register_extended_taxonomy('newsflow-category', ['newsflow-source','newsflow-item','newsflow-config'], [

                ], [
                    'singular' => 'Category',
                    'plural' => 'Categories',
                ]);

                \register_extended_taxonomy('newsflow-channel', ['newsflow-source','newsflow-item','newsflow-config'], [

                ], [
                    'singular' => 'Channel',
                    'plural' => 'Channels',
                ]);
            }

        });
    }
}
