<?php

namespace newsflow\lib;

class admin
{
    public function __construct()
    {
        add_action('admin_menu', array( &$this, 'menues' ));
        add_action('parent_file', array( &$this, 'parent_file' ));
        add_filter('page_row_actions', array( &$this, 'add_fetch_link' ), 10, 2);
    }

    public function add_fetch_link($actions, $page_object)
    {
        if ($page_object->post_type == "newsflow-source") {
            $actions['fetch_link'] = '<a target="_blank" href="' . admin_url('admin-ajax.php') . '?action=newsflow-run&id=' . $page_object->ID . '">Fetch</a>';
        }
        return $actions;
    }

    public function menues()
    {
        add_menu_page('Newsflow', 'Newsflow', 'edit_others_posts', 'newsflow-admin-inbox',
            __NAMESPACE__ . '\Inbox::display', '', 3);

        add_submenu_page('newsflow-admin-inbox', 'Sources', 'Sources', 'edit_others_posts',
            'edit.php?post_type=newsflow-source');
        add_submenu_page('newsflow-admin-inbox', 'Categories', 'Categories', 'edit_others_posts',
            'edit-tags.php?taxonomy=newsflow-category');
        add_submenu_page('newsflow-admin-inbox', 'Channels', 'Channels', 'edit_others_posts',
            'edit-tags.php?taxonomy=newsflow-channel');
        add_submenu_page('newsflow-admin-inbox', 'Items', 'Items', 'edit_others_posts',
            'edit.php?post_type=newsflow-item');
        add_submenu_page('newsflow-admin-inbox', 'Configurations', 'Configurations', 'edit_others_posts',
            'edit.php?post_type=newsflow-config');

        //if( isset($settings['menu_contracts']) && $settings['menu_contracts'] ) add_submenu_page( 'newsflow-admin-inbox', 'Contracts', 'Contracts', 'edit_others_posts', 'edit-tags.php?taxonomy=newsflow-contract' );
    }

    public function parent_file($parent_file)
    {
        global $current_screen;
        $taxonomy  = $current_screen->taxonomy;
        $post_type = $current_screen->post_type;
        $papi_page = papi_get_page_type_id(get_the_ID());
        if (
            $post_type == 'newsflow-source' ||
            $post_type == 'newsflow-item' ||
            $post_type == 'newsflow-config' ||
            $taxonomy == 'newsflow-category' ||
            $taxonomy == 'newsflow-channel' ||
            $papi_page == 'feed' ||
            $papi_page == 'magic'
        ) {
            $parent_file = 'newsflow-admin-inbox';
        }

        return $parent_file;
    }
}

new Admin();
