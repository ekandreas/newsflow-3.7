<?php

namespace newsflow\lib;

class loader
{

    private $sources;

    public function load_sources($max_active = 1)
    {
        $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : 0;

        if (! $id) {
            $args = [
                'post_type'      => SourceBase::POST_TYPE,
                'posts_per_page' => $max_active,
                'meta_query'     => array(
                    array(
                        'key'     => 'status',
                        'compare' => 'NOT EXISTS',
                    ),
                ),
                'order'          => 'ASC',
                'meta_key'       => 'fetched',
                'orderby'        => 'meta_value_num',
            ];

            $sources = get_posts($args);

            if ($sources) {
                foreach ($sources as $source) {
                    $this->sources[] = $source;
                }
            }
        } else {
            $source = get_post($id);
            if ($source) {
                $this->sources[] = $source;
            }
        }
    }

    public function fetch()
    {
        if ($this->sources && sizeof($this->sources)) {
            foreach ($this->sources as $post) {
                $source = SourceFactory::get_source_object($post);

                try {
                    $source->mark_started();

                    $source->pre_fetch();
                    $source->fetch();
                    $source->post_fetch();
                } catch (Exception $ex) {
                }
            }
        }
    }
}
