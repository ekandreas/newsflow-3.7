<?php

class testUserGate extends WP_Ajax_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testBlogs()
    {
        require_once dirname(__DIR__).'/../vendor/autoload.php';

        // create user
        $user_id = wp_create_user('arne@aekab.se', '123123', 'arne@aekab.se');
        $this->assertTrue($user_id>0);

        // create blog
        $blog_id = $this->factory->blog->create([
            'domain' => 'example.org',
            'path' => 'test/',
        ]);
        
        // add user to blog
        add_user_to_blog($blog_id, $user_id, 'subscriber');

        // validate url
        $sites = Newsflow\UI\UserGate::currentUserSites($user_id);
        $this->assertCount(1, $sites);

        wp_set_current_user($user_id);


        $_POST['nonce'] = wp_create_nonce('error_gatekeeper');
        try {
            $this->_handleAjax('getUserUrl');
        } catch (Exception $e) {
        }
        $response = json_decode($this->_last_response);

        $this->assertEquals('error', $response->status);

        $this->_last_response='';
        $_POST['nonce'] = wp_create_nonce('gatekeeper');
        try {
            $this->_handleAjax('getUserUrl');
        } catch (Exception $e) {
        }
        $response = json_decode($this->_last_response);

        $this->assertEquals('success', $response->status);
        $this->assertEquals('test/', $response->data);

        // create another blog
        $blog_id = $this->factory->blog->create([
            'domain' => 'example.org',
            'path' => 'test2/',
        ]);

        // add user to the new blog
        add_user_to_blog($blog_id, $user_id, 'subscriber');

        // validate array with blogs
        $sites = Newsflow\UI\UserGate::currentUserSites($user_id);
        $this->assertCount(2, $sites);
    }
}
